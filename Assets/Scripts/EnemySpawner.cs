﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour
{
	public GameObject enemy;
	//public bool Condition;

	void Start()
	{
		InvokeRepeating("SpawnEnemy", 0, 30);
	}

	void SpawnEnemy()
	{
		Instantiate(enemy, transform.position, transform.rotation);
	}
}