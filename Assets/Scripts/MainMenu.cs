﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour
{
	//public bool isMulti;
	public bool isQuit;
	
	void OnMouseEnter()
	{
		renderer.material.color = Color.grey; 
	}
	
	void OnMouseExit()
	{
		renderer.material.color = Color.cyan;
	}
	
	void OnMouseDown()
	{
		if(isQuit)
		{
			Application.Quit();  
		}
		else
		{
			//if (isMulti) { IsMultiplayer.SetToMultiplayer(); }
			//else { IsMultiplayer.SetToSingleplayer(); }
			
			Application.LoadLevel("MainScene");
		}
	}
}