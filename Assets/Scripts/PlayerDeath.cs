﻿using UnityEngine;
using System.Collections;

public class PlayerDeath : MonoBehaviour
{
	public float health = 100f;                         // How much health the player has left.
	public float resetAfterDeathTime = 5f;              // How much time from the player dying to the level reseting.
	public AudioClip deathClip;                         // The sound effect of the player dying.

	private SceneFadeInOut sceneFadeInOut;              // Reference to the SceneFadeInOut script.
	private HandleInCombat handleInCombat;              // Reference to the LastPlayerSighting script.
	private float timer;                                // A timer for counting to the reset of the level once the player is dead.
	private bool playerDead;                            // A bool to show if the player is dead or not.
	
	
	void Awake ()
	{
		// Setting up the references.
		//playerMovement = GetComponent<PlayerMovement>();
		sceneFadeInOut = GameObject.FindGameObjectWithTag(Tags.fader).GetComponent<SceneFadeInOut>();
		handleInCombat = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<HandleInCombat>();
	}
	
	
	void Update ()
	{
		// If health is less than or equal to 0...
		if(health <= 0f)
		{
			// ... and if the player is not yet dead...
			if(!playerDead)
				// ... call the PlayerDying function.
				PlayerDying();
			else
			{
				// Otherwise, if the player is dead, call the PlayerDead and LevelReset functions.
				PlayerDead();
				LevelReset();
			}
		}
	}
	
	
	void PlayerDying()
	{
		// The player is now dead.
		playerDead = true;		
		// Play the dying sound effect at the player's location.
		//AudioSource.PlayClipAtPoint(deathClip, transform.position);
	}
	
	
	void PlayerDead ()
	{	
		// Disable the movement.
		//playerMovement.enabled = false;		
		// Reset the player sighting to turn off the alarms.
		HandleInCombat.position = HandleInCombat.resetPosition;
		
		// Stop the footsteps playing.
		audio.Stop();
	}
	
	
	void LevelReset ()
	{
		// Increment the timer.
		timer += Time.deltaTime;
		
		//If the timer is greater than or equal to the time before the level resets...
		if (timer >= resetAfterDeathTime)
		{// ... reset the level.
			sceneFadeInOut.EndScene ();
			playerDead = false;
		}
	}
	
	
	public void TakeDamage (float amount)
	{	// Decrement the player's health by amount.
		health -= amount;
	}
}