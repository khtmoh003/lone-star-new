﻿using UnityEngine;
using System.Collections;

public class PlayerDetection : MonoBehaviour
{
	public GameObject player;																// Reference to the player.
	public static Vector3 lastPlayerSighting = new Vector3(100000f, 100000f, 100000f);		// Reference to the global last sighting of the player.
	
	void OnTriggerStay(Collider other)
	{
		// If the colliding gameobject is the player...
		if(other.gameObject == player)
		{
			// ... raycast from the camera towards the player.
			Vector3 relPlayerPos = player.transform.position - transform.position;
			RaycastHit hit;
			
			if(Physics.Raycast(transform.position, relPlayerPos, out hit))
			{	// If the raycast hits the player...
				if(hit.collider.gameObject == player)
				{	// ... set the last global sighting of the player to the player's position.
					lastPlayerSighting = player.transform.position;
				}
			}
		}
	}

	void OnTriggerExit(Collider other)
	{
		if(other.gameObject == player)
		{
			lastPlayerSighting = new Vector3(100000f, 100000f, 100000f); 
		}
	}
}